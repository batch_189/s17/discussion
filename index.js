console.log('hello')

// To declare the function
// 1. function keyword
// 2. function name
// 3. open/close ()
// 4. ()
function sayHello() {
    console.log('Hello there!');
}
// to call the function in order to print it in console
// you can invoke a function by calling its function name nad including the ()
sayHello();

// you ca assign a function to a variable. the function name would not be required
let sayGoodbye = function() {
    console.log('Goodbye!');
}
// you can invoke a function inside a variable by using the variable name
sayGoodbye();

// you can aslso re-assign a function as a new value of a variable
sayGoodbye = function() {
    console.log('Au Revoir');
}
sayGoodbye();

// declaring a const var with a function as a value will not allow that function to be changed or re-assigned
const sayHelloInJapanese = function() {
    console.log('Ohayo');
}
sayHelloInJapanese();

/*
this will give error since const cannot ne re-assigned
sayHelloInJapanese = function() {
    console.log('kamusta');
}
sayHelloInJapanese()

*/

// GLOBAL scope - can be use anywhere
let action = 'Run'

function doSomethingRandom() {
    // LOCAL/FUNCTION scope - only inside a function
    console.log(action);
}
doSomethingRandom();


// NESTED functions

/*
You can nest a function inside a function as long as you 
invoke the child function within the scope of the parent function.
*/
function viewProduct() {
    console.log('Viewing a product')
    function addToCart() {
        console.log('Added product to cart')
    }
    addToCart(); 
}
viewProduct();


// Alert function is a built-in javascript function where we can show alerts to the user.
function greet() {
    alert('Hey You');
}
greet();
// will run only after the alert has been closed if it is invoke below of the alert function.
console.log('How are you?')


// Prompt is a built in javascript function that we can use to take input from the user.
function enterUserName() {
    let username = prompt ('Enter your username');
    console.log('userName');
}   
enterUserName();

// you cane use a prompt of a function. make sure to output the prompt value by assigning to a vari able and using console.log()
console.log(prompt ('Enter you Age'))